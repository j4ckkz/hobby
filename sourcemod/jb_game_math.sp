#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <jb_game>

public Plugin myinfo = {
    name = "JB: Last Request - Math",
    author = "Xalus, X3",
    description = "Math",
    version = "1.0",
    url = "neondragon.net"
};

char g_strAnswer[6];
int answerA, answerB, answerC, answerD, intType;

char chatString[124];
bool g_bInGame;

int g_iPlayer[2];

public void OnPluginStart() {
    jb_add_game(LR_MATH, "Math Contest", 0, "", none);

    AddCommandListener(playerSay, "say");
}

public void none() { 
    return;
}

public jb_game_started(int gameid, int id, int guard, int type) {
    if(gameid == LR_MATH) {

        g_bInGame = true;

        g_iPlayer[0] = id;
        g_iPlayer[1] = guard;
        
        intType = GetRandomInt(0, 2);

        switch(intType) {
            case 0: {
                answerA = GetRandomInt(1, 99);
                answerB = GetRandomInt(1, 99);

                IntToString( (answerA + answerB), g_strAnswer, sizeof(g_strAnswer) );

                FormatEx(chatString, sizeof(chatString), "%i + %i ?", answerA, answerB); 
            }
            case 1: {
                answerA = GetRandomInt(1, 99);
                answerB = GetRandomInt(1, 99);
                answerC = GetRandomInt(1, 99);
                answerD = GetRandomInt(1, 99);

                IntToString( (answerA + answerB + answerC + answerD), g_strAnswer, sizeof(g_strAnswer));

                FormatEx(chatString, sizeof(chatString), "%i + %i + %i + %i ?", answerA, answerB, answerC, answerD); 
            }
            case 2: {
                answerA = GetRandomInt(1, 9);
                answerB = GetRandomInt(1, 9);

                IntToString( (answerA * answerB), g_strAnswer, sizeof(g_strAnswer));

                FormatEx(chatString, sizeof(chatString), "%i * %i ?", answerA, answerB); 
            }
        }
        Handle hHudText = CreateHudSynchronizer();

        SetHudTextParams(-1.0, 0.5, 5.0, 255, 0, 0, 255, 1);
        ShowSyncHudText(id, hHudText, "Get Ready \nThe equation is..");
        ShowSyncHudText(guard, hHudText, "Get Ready \nThe equation is..");
        CloseHandle(hHudText);

        DataPack data;
        CreateDataTimer(5.0, mathReveal, data);
        data.WriteCell(id);
        data.WriteCell(guard);
    }
}

public jb_game_finished(int gameid, int winner, int loser, int gametype) {

    if(gameid == LR_MATH) {
        g_bInGame = false;
        g_strAnswer[0] = EOS;
    }
}

public Action mathReveal(Handle timer, DataPack data) {

    int terr, ct;
    data.Reset();
    terr = data.ReadCell();
    ct = data.ReadCell();

    Handle hHudText = CreateHudSynchronizer();
    SetHudTextParams(-1.0, 0.7, 5.0, 255, 0, 0, 255, 1);
    ShowSyncHudText(terr, hHudText, chatString);
    ShowSyncHudText(ct, hHudText, chatString);
    CloseHandle(hHudText);
}

public Action playerSay(int id, const char[] command, int argc) {

    if(g_bInGame) {
        char sArgs[6];
        GetCmdArgString(sArgs, sizeof(sArgs));
        StripQuotes(sArgs);

        if(g_iPlayer[0] == id || g_iPlayer[1] == id) {
            if(StrEqual(g_strAnswer, sArgs)) {
                SDKHooks_TakeDamage( (g_iPlayer[0] == id) ? g_iPlayer[1] : g_iPlayer[0], id, id, 200.0, DMG_SHOCK);
            }
        }
    }

}
