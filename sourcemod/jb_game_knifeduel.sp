#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <jb_game>


public Plugin myinfo = {
    name = "JB: Last Request Knife Duel",
    author = "Xalus, X3",
    description = "Knife",
    version = "1.0",
    url = "neondragon.net"
};

int iHp

public void OnPluginStart() {
    jb_add_game(LR_KNIFEDUEL, "Knife Duel", (TERGAME_VIEWSYSTEM | TERGAME_FRIENDLYFIRE), "Mode", Knifeduel_Modemenu);
}

public Knifeduel_Modemenu(int client) {

    if(client <= 0 || !IsValidEntity(client) || !IsPlayerAlive(client))
        return;
    
    Menu intMenu = new Menu(KnifeDuelHandler, MENU_ACTIONS_ALL);
    intMenu.SetTitle("Select Mode");

    intMenu.AddItem("fullhp", "100 HP");
    intMenu.AddItem("35hp", "35 HP");
    intMenu.AddItem("2hp", "2 HP");
    intMenu.Display(client, 100);
}

public int KnifeDuelHandler(Menu intMenu, MenuAction action, int param1, int param2) {
    if(param1 <= 0 || !IsValidEntity(param1) || !IsClientInGame(param1)) 
        return;


    if(action == MenuAction_Select) {
        char info[32];
        intMenu.GetItem(param2, info, sizeof(info));
        
        if(StrEqual(info, "fullhp")) {
            jb_set_subname(param1, param2, "100 Health");
            iHp = 100;
        }

        else if(StrEqual(info, "35hp")) {
            jb_set_subname(param1, param2, "35 Health");
            iHp = 35;
        }

        else if(StrEqual(info, "2hp")) {
            jb_set_subname(param1, param2, "2 Health");
            iHp = 2;
        }
    }
}

public jb_game_started(int gameid, int id, int guard, int type) {

    if(gameid == LR_KNIFEDUEL) {

        SetEntityHealth(id, iHp);
        SetEntityHealth(guard, iHp);

        int weaponIndexCT, weapondIndexT
        for(int i = 0; i < 2; i++) {

            while( (weapondIndexT = GetPlayerWeaponSlot(id, i)) != -1) {
                CS_DropWeapon(id, weapondIndexT, false, true);
                AcceptEntityInput(weapondIndexT, "Kill");
            }

            while( (weaponIndexCT = GetPlayerWeaponSlot(guard, i)) != -1 ) {
                CS_DropWeapon(guard, weaponIndexCT, false, true);
                AcceptEntityInput(weaponIndexCT, "Kill");
            }
        }
        SetEntProp(id, Prop_Send, "m_ArmorValue", 0, 1);
        SetEntProp(guard, Prop_Send, "m_ArmorValue", 0, 1);
    }
}