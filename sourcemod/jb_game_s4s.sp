#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <jb_game>

public Plugin myinfo = {
    name = "JB: Last Request - Shot 4 Shot",
    author = "Xalus, X3",
    description = "Shot 4 Shot",
    version = "1.0",
    url = "neondragon.net"
};

enum struct weaponlist_enum {
    char WEAPONLIST_WEAPNAME[32];
    int WEAPONLIST_WEAPAMMO;
    char WEAPONLIST_WEAPNAME_FANCY[32];
    bool WEAPONLIST_SECONDARY;
}

#define WEAPONLIST_AMOUNT 2

weaponlist_enum g_weaponlist[WEAPONLIST_AMOUNT] = {
    {"weapon_deagle", 35, "Deagle", true},
    {"weapon_ssg08", 90, "Scout", false}
}

public void OnPluginStart() {
    jb_add_game(LR_S4S, "Shot for Shot", (TERGAME_SLOWSYSTEM | TERGAME_VIEWSYSTEM | TERGAME_FRIENDLYFIRE), "Weapon", ShotForShot_Gunmenu);
}

public jb_game_started(int gameid, int id, int guard, int type, int subtype) {
    if(gameid == LR_S4S) {
        s4s_Weapon(id, subtype);
        s4s_Weapon(guard, subtype);
    }
}

stock s4s_Weapon(int id, int subtype) {

    if(id > 0 && IsValidEntity(id) && IsClientInGame(id)) {

        int weaponIndex = g_weaponlist[subtype].WEAPONLIST_SECONDARY ? 1 : 0;
        int weaponIndexHandler

        weaponIndexHandler = GetPlayerWeaponSlot(id, weaponIndex)

        if(weaponIndexHandler > -1) {

            CS_DropWeapon(id, weaponIndexHandler, false, true);
            AcceptEntityInput(weaponIndexHandler, "Kill");
        }
        GivePlayerItem(id, g_weaponlist[subtype].WEAPONLIST_WEAPNAME);
        SetReserveAmmo(id, g_weaponlist[subtype].WEAPONLIST_WEAPAMMO, g_weaponlist[subtype].WEAPONLIST_SECONDARY ? true : false)
    }
}

stock SetReserveAmmo(int client, int ammo, bool secondary)
{
    new weapon = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
    if(weapon < 1) return;
    
    new ammotype = GetEntProp(weapon, Prop_Send, secondary ? "m_iSecondaryAmmoType" : "m_iPrimaryAmmoType");
    if(ammotype == -1) return;
    
    SetEntProp(client, Prop_Send, "m_iAmmo", ammo, _, ammotype);
}

public ShotForShot_Gunmenu(int client) {
    Menu intMenu = new Menu(hShotForShot, MENU_ACTIONS_ALL);

    intMenu.SetTitle("Select Weapons");

    for(int i = 0; i < WEAPONLIST_AMOUNT; i++) {

        char strKey[2];
        IntToString(i, strKey, sizeof(strKey));
        intMenu.AddItem(strKey, g_weaponlist[i].WEAPONLIST_WEAPNAME_FANCY);
    }
    intMenu.Display(client, 25);
}

public int hShotForShot(Menu intMenu, MenuAction action, int param1, int param2) {
    if(param1 <= 0 || !IsValidEntity(param1) || !IsClientInGame(param1)) 
        return;

    if(action == MenuAction_Select) {
        char info[32], display[32];
        int style;
        intMenu.GetItem(param2, info, sizeof(info), style, display, sizeof(display));
    
        param2 = StringToInt(info);

        jb_set_subname(param1, param2, g_weaponlist[param2].WEAPONLIST_WEAPNAME_FANCY);
    }
}

