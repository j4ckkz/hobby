#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <cstrike>
#include <colors>

#include <jb_game_const>

#pragma semicolon 1

public Plugin myinfo =
{
    name = "JB: Last Request API",
    author = "Xalus, X3",
    description = "Games API",
    version = "1.0",
    url = "neondragon.net"
};


enum struct cvar_enum {
    ConVar CVAR_STATUSLR;
    ConVar CVAR_STATUSTG;
    ConVar CVAR_MAXPLAYS;
    ConVar CVAR_TIMELR;
    ConVar CVAR_SLOWLIMIT;
}

cvar_enum g_cvar_list;

enum struct setting_enum {
    int SETTING_STATUS;
    int SETTING_GAME;

    int SETTING_TYPE;
    // 0 = LR
    // 1 = TGAME
    int SETTING_VIEW;
    int SETTING_CHANGING;

    int SETTING_SLOWPLAYS;
    int SETTING_PRISONER;
    int SETTING_GUARD;

	int SETTING_SUB_ID;
	char SETTING_SUB_NAME[32];
}

enum struct tergame_enum {
	char TERGAME_NAME[32];
    char TERGAME_SUBMENU[64];
	Function TERGAME_SUBMENU_FUNCTION;
	int TERGAME_FLAGS;
	Handle TERGAME_SUBMENU_PLUGINID;
}
setting_enum g_setting_list;

GlobalForward g_intForwardFinish, g_intForwardStarted;
bool g_firstround_block = true;

tergame_enum g_tergame_list[LR_MAX];
int g_tergame_plays[LR_MAX];

int gamesr;
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max) {
    CreateNative("jb_add_game", Native_AddGame);
    CreateNative("jb_get_runninggame", Native_RunningGame);
    CreateNative("jb_set_subname", Native_Subname);

    return APLRes_Success;
}

public void OnPluginStart() {

    // Register status cvars
    g_cvar_list.CVAR_STATUSLR = CreateConVar("jb_lr_status", "1", "Enable/Disable LastRequest.");
    g_cvar_list.CVAR_STATUSTG = CreateConVar("jb_games_status", "1");

    g_cvar_list.CVAR_MAXPLAYS = CreateConVar("jb_lr_maxplays", "6");
    g_cvar_list.CVAR_SLOWLIMIT = CreateConVar("jb_lr_slowlimit", "2");

    g_cvar_list.CVAR_TIMELR = CreateConVar("jb_lr_timer", "45");

    RegConsoleCmd("lr", onPlayerLRCommand);
    RegConsoleCmd("tgames", onPlayerTGCommand);

    HookEvent("player_death", onPlayerDeath);
    HookEvent("round_end", onRoundEnd);

    RegConsoleCmd("test", onTest);
    
    // gameid, terrorist, ct, type (LR / Tgames), subtype
    g_intForwardStarted = new GlobalForward("jb_game_started", ET_Event, Param_Cell, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
    // gameid, winner, looser, type (LR / Tgames)
    g_intForwardFinish = new GlobalForward("jb_game_finished", ET_Event, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
}

public Action onTest(int client, int args) {
    PrintToChatAll("%i, %s", gamesr, g_tergame_list[LR_HEBATTLE].TERGAME_NAME);
}

public int Native_Subname(Handle plugin, int numParams) {
    int subid;
    subid = GetNativeCell(2);

    if(subid >= 0) {
        //g_setting_list.SETTING_SUB_ID = subid;
        g_setting_list.SETTING_SUB_ID = subid;
        GetNativeString(3, g_setting_list.SETTING_SUB_NAME, 31);
    }
    else {
        if(!g_setting_list.SETTING_SUB_NAME) {
            g_setting_list.SETTING_SUB_ID = 0;
            GetNativeString(3, g_setting_list.SETTING_SUB_NAME, 31);
        }
    }

    if(g_setting_list.SETTING_TYPE && GetAliveTeamCount(CS_TEAM_T) == 2) {

        int index1, index2;
        for(int terr = 1; terr <= MaxClients; terr++) {
            if(IsClientInGame(terr)
            && IsPlayerAlive(terr)
            && GetClientTeam(terr) == CS_TEAM_T) {

                if(index1 > 1)
                    index2 = terr;
                else
                    index1 = terr;
            }
        }
        set_lastrequest(index1, index2, g_setting_list.SETTING_GAME, g_setting_list.SETTING_PRISONER);
    }
    else {
        Tergame_Menu(GetNativeCell(1));
    }
}

public int Native_AddGame(Handle plugin, int numParams) {
    int intGameid;
    intGameid = GetNativeCell(1);

    GetNativeString(2, g_tergame_list[intGameid].TERGAME_NAME, 31);

    if(numParams >= 3) {
        g_tergame_list[intGameid].TERGAME_FLAGS = GetNativeCell(3);

        if(numParams >= 4) {
            GetNativeString(4, g_tergame_list[intGameid].TERGAME_SUBMENU, 63);
            g_tergame_list[intGameid].TERGAME_SUBMENU_FUNCTION = GetNativeFunction(5);
            g_tergame_list[intGameid].TERGAME_SUBMENU_PLUGINID = plugin;
        }
    }
    gamesr++;
}

public int Native_RunningGame(Handle plugin, int numParams) {
    if(g_setting_list.SETTING_STATUS) {
        return g_setting_list.SETTING_GAME;
    }
    return -1;
}

public OnClientDisconnect(int client) {
	if(g_setting_list.SETTING_STATUS
	&& (g_setting_list.SETTING_PRISONER == client || g_setting_list.SETTING_GUARD == client) ) {
        lastrequest_finish(client);
    }
}

public Action onPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {

    int iVictim = GetClientOfUserId(GetEventInt(event, "userid"));

    if(g_setting_list.SETTING_STATUS && (g_setting_list.SETTING_PRISONER == iVictim || g_setting_list.SETTING_GUARD == iVictim)) {
        lastrequest_finish(iVictim);
    }
}

public Action onRoundEnd(Handle:event, const String:name[], bool:dontBroadcast) {

    if(g_setting_list.SETTING_STATUS
    && !g_setting_list.SETTING_GUARD) {
        lastrequest_finish(!IsPlayerAlive(g_setting_list.SETTING_PRISONER) ? g_setting_list.SETTING_PRISONER : g_setting_list.SETTING_GUARD);
    }

    for(int i = 0; i < LR_MAX; i++) {
        g_tergame_plays[i] = 0;
    }
    g_setting_list.SETTING_SLOWPLAYS = 0;

    g_firstround_block = false;
}


public Action onPlayerLRCommand(int client, int args) {

    if(client <= 0 || !IsValidEntity(client) || !IsClientInGame(client)) 
        return Plugin_Handled;

    if(!GetConVarInt(g_cvar_list.CVAR_STATUSLR) || g_firstround_block) {
        PrintToChat(client, "LRs are disabled ATM. Try again later");
        return Plugin_Handled;
    }

    if(lastrequest_ready(client)) {

        g_setting_list.SETTING_TYPE = 0;
        g_setting_list.SETTING_VIEW = 0;
        g_setting_list.SETTING_CHANGING = 0;
        menuGames(client);
    }
    return Plugin_Continue;
}

public Action onPlayerTGCommand(int client, int args) {

    if(client <= 0 || !IsValidEntity(client) || !IsClientInGame(client)) 
        return Plugin_Handled;

    if(!GetConVarInt(g_cvar_list.CVAR_STATUSTG) || g_firstround_block) {
        PrintToChat(client, "Tgames are disabled ATM. Try again later");
        return Plugin_Handled;
    }
    if(IsPlayerAlive(client) && GetClientTeam(client) == CS_TEAM_CT) {

        if(GetAliveTeamCount(CS_TEAM_T) == 2) {
            g_setting_list.SETTING_TYPE = 1;
            g_setting_list.SETTING_VIEW = 0;
            g_setting_list.SETTING_CHANGING = 0;
            menuGames(client);
        }
    }
    return Plugin_Handled;
}

public menuGames(int client) {


    if(client <= 0 || !IsValidEntity(client) || !IsClientInGame(client)) {
        PrintToChatAll("not valid");
        return;
    }
    
    g_setting_list.SETTING_PRISONER = client;
    int cvarSlow = GetConVarInt(g_cvar_list.CVAR_SLOWLIMIT);
    char strTemp[101];

    FormatEx(strTemp, sizeof(strTemp), "Choose your game:\n %i Slow Plays Left", (cvarSlow - g_setting_list.SETTING_SLOWPLAYS));

    Menu intMenu = new Menu(hMenuGames, MENU_ACTIONS_ALL);  
    intMenu.SetTitle(strTemp);

    int cvarPlays = GetConVarInt(g_cvar_list.CVAR_MAXPLAYS);
    int bVIP = is_user_vip(client);

    char strKey[6];
    for(int i; i < LR_MAX; i++) {
        if(g_tergame_plays[i] < cvarPlays 
        && g_tergame_list[i].TERGAME_NAME[0]) {
            
            if(g_setting_list.SETTING_TYPE && 
            g_tergame_list[i].TERGAME_FLAGS & TERGAME_VSGUARDS) {
                continue;
            }

            // Slow plays system
            if(!g_setting_list.SETTING_TYPE
            && g_tergame_list[i].TERGAME_FLAGS & TERGAME_SLOWSYSTEM
            && g_setting_list.SETTING_SLOWPLAYS >= cvarSlow) {
                continue;
            }
            
            IntToString(i, strKey, sizeof(strKey));

            if(g_tergame_list[i].TERGAME_FLAGS & TERGAME_VIP) {
                if(bVIP) {
                    FormatEx(strTemp, sizeof(strTemp), "[V.I.P] %s", g_tergame_list[i].TERGAME_NAME);
                    intMenu.AddItem(strKey, strTemp);
                }
                continue;
            }
            FormatEx(strTemp, sizeof(strTemp), "%s", g_tergame_list[i].TERGAME_NAME);
            intMenu.AddItem(strKey, strTemp);
        }
    }
    intMenu.Display(client, 25);
}
public int hMenuGames(Menu intMenu, MenuAction action, int param1, int param2)
{
    if(param1 <= 0 || !IsValidEntity(param1) || !IsClientInGame(param1)) 
        return;
    
    char info[32], display[32];
    int style;
    intMenu.GetItem(param2, info, sizeof(info), style, display, sizeof(display));

    param2 = StringToInt(info);

    if(action == MenuAction_Select) {
        bool submenu_force;

        if(g_setting_list.SETTING_CHANGING
        && g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU[0] != g_setting_list.SETTING_GAME) {
            g_setting_list.SETTING_SUB_NAME[0] = EOS;
            g_setting_list.SETTING_SUB_ID = 0;

            submenu_force = true;
        }

        g_setting_list.SETTING_GAME = param2;

        if(g_setting_list.SETTING_TYPE) {

            PrintToChatAll("tgames");
            
            if(GetAliveTeamCount(CS_TEAM_T) == 2) {
                PrintToChatAll("ok, inside function");
                if(g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU[0]) {
                    g_setting_list.SETTING_PRISONER = param1;
                    SubMenu_Forward(param1);
                }
                else {

                    int index1, index2;
                    for(int terr = 1; terr <= MaxClients; terr++) {
                        PrintToChatAll("in loop");
                        if(IsValidEntity(terr)
                        && IsPlayerAlive(terr)
                        && GetClientTeam(terr) == CS_TEAM_T) {

                            if(index1 > 1)
                                index2 = terr;
                            else
                                index1 = terr;
                        }
                    }
                    PrintToChatAll("%i, %i", index1, index2);
                    set_lastrequest(index1, index2, g_setting_list.SETTING_GAME, param1);
                }
            }
            return;
        }

        if(!lastrequest_ready(param1)) 
            return;
        
        if(g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_FLAGS & TERGAME_VSGUARDS) {
            set_lastrequest(param1, 0, g_setting_list.SETTING_GAME);
            return;
        }
        if(submenu_force && g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU[0]) {
            SubMenu_Forward(param1);
        }
        else if(g_setting_list.SETTING_CHANGING) {
            Tergame_Menu(param1);
        }
        else {
            PlayerList_Menu(param1);
        }
    }
}

public PlayerList_Menu(int Client) {

    if(Client <= 0 || !IsValidEntity(Client) || !IsClientInGame(Client)) 
        return;
    

    Menu intMenu = new Menu(hPlayerListMenu, MENU_ACTIONS_ALL);

    intMenu.SetTitle("Choose your opponent");

    char strName[32], strKey[6];
    for(int i = 1; i <= MaxClients; i++) {
        if(IsClientConnected(i) && IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == CS_TEAM_CT) {
            
            GetClientName(i, strName, sizeof(strName));
            IntToString(i, strKey, sizeof(strKey));
            intMenu.AddItem(strKey, strName);
        }
    }
    intMenu.Display(Client, 30);    
}

public int hPlayerListMenu(Menu intMenu, MenuAction action, int param1, int param2) {

    if(param1 <= 0 || !IsValidEntity(param1) || !IsClientInGame(param1)) 
        return;
    

    char info[32], display[32];
    int style;
    intMenu.GetItem(param2, info, sizeof(info), style, display, sizeof(display));

    param2 = StringToInt(info);
    
    if(action == MenuAction_Select) {

        if(lastrequest_ready(param1)) {

            g_setting_list.SETTING_GUARD = param2;

            if(!g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU[0]) {

                Tergame_Menu(param1);
            }
            else {
                SubMenu_Forward(param1);
            }
        }
    }
}

public SubMenu_Forward(int Client) {

    if(Client <= 0 || !IsValidEntity(Client) || !IsClientInGame(Client)) 
        return;

    PrivateForward fSubFunction = new PrivateForward(ET_Event, Param_Cell);

    fSubFunction.AddFunction(g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU_PLUGINID, g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU_FUNCTION);


    Action intReturn;
    Call_StartForward(fSubFunction);
    Call_PushCell(Client);
    Call_Finish(intReturn);
}


public Tergame_Menu(int client) {

    if(client <= 0 || !IsValidEntity(client) || !IsClientInGame(client)) 
        return;

    if(!lastrequest_ready(client)) {
        return;
    }

    Menu intMenu = new Menu(hTerroristGame, MENU_ACTIONS_ALL);
    intMenu.SetTitle("Last request:");

    char menu_format[101];
    FormatEx(menu_format, sizeof(menu_format), "Game: %s", g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_NAME);
    intMenu.AddItem("1", menu_format);

    char guard_name[MAX_NAME_LENGTH];
    if(IsPlayerAlive(g_setting_list.SETTING_GUARD)) {
        GetClientName(g_setting_list.SETTING_GUARD, guard_name, sizeof(guard_name));
    }
    else {
        guard_name = "Error";
    }

    FormatEx(menu_format, sizeof(menu_format), "Guard: %s", guard_name);
    intMenu.AddItem("2", menu_format);

    if(g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU[0]) {
        FormatEx(menu_format, sizeof(menu_format), "%s: %s", g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU, g_setting_list.SETTING_SUB_NAME);
        intMenu.AddItem("3", menu_format);
    }

    if(g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_FLAGS & TERGAME_VIEWSYSTEM 
    && g_setting_list.SETTING_SLOWPLAYS < GetConVarInt(g_cvar_list.CVAR_SLOWLIMIT)) {
        FormatEx(menu_format, sizeof(menu_format), "View: %s %s \n", g_setting_list.SETTING_VIEW ? "3RD" : "Normal", !is_user_vip(client) ? "(VIP)" : "");
        intMenu.AddItem("4", menu_format);
    }

    intMenu.AddItem("5", "Start");
    intMenu.Display(client, 25);
}

public int hTerroristGame(Menu intMenu, MenuAction action, int param1, int param2) {

    if(param1 <= 0 || !IsValidEntity(param1) || !IsClientInGame(param1)) 
        return;
    
    char info[32], display[32];
    int style;
    intMenu.GetItem(param2, info, sizeof(info), style, display, sizeof(display));

    param2 = StringToInt(info);

    if(action == MenuAction_Select) {
        g_setting_list.SETTING_CHANGING = 1;

        switch(param2) {
            case 1: menuGames(param1);
            case 2: PlayerList_Menu(param1);
            case 3: SubMenu_Forward(param1);
            case 4: {
                if(is_user_vip(param1)) {
                    g_setting_list.SETTING_VIEW = !g_setting_list.SETTING_VIEW;
                }
                Tergame_Menu(param1);
            }
            case 5: {
                if(lastrequest_ready(param1)) {
                    set_lastrequest(param1, g_setting_list.SETTING_GUARD, g_setting_list.SETTING_GAME);
                }
            }
        }
    }
}


stock Action lastrequest_finish(int id) {
    char strName[2][32];
    GetClientName(g_setting_list.SETTING_PRISONER, strName[0], 31);

    if(!g_setting_list.SETTING_GUARD) {
        strcopy(strName[1], 31, "Guards");
        SetEntityRenderMode(g_setting_list.SETTING_PRISONER, RENDER_NORMAL);
        SetEntityRenderColor(g_setting_list.SETTING_PRISONER);
    }
    else {
        GetClientName(g_setting_list.SETTING_GUARD, strName[1], 31);
        SetEntityRenderMode((id == g_setting_list.SETTING_PRISONER) ? g_setting_list.SETTING_GUARD : g_setting_list.SETTING_PRISONER, RENDER_NORMAL);
        SetEntityRenderColor((id == g_setting_list.SETTING_PRISONER) ? g_setting_list.SETTING_GUARD : g_setting_list.SETTING_PRISONER);

        if(g_setting_list.SETTING_VIEW) {
            if(IsClientConnected(g_setting_list.SETTING_PRISONER)) {
                SetFirstPerson(id);
            }
            else if(IsClientConnected(g_setting_list.SETTING_GUARD)) {
                SetFirstPerson(id);
            }
        }
    }

    CPrintToChatAll("{green}>nD>{red} %s{default} has %s the {red}%s{default} against {red}%s", strName[0], (id == g_setting_list.SETTING_PRISONER) ? "lost" : "won", g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_NAME, strName[1]);

    ServerCommand("sm_cvar jb_tk 0");
    Action forwardResult;

    Call_StartForward(g_intForwardFinish);

    Call_PushCell(g_setting_list.SETTING_GAME);
    Call_PushCell((id == g_setting_list.SETTING_PRISONER) ? g_setting_list.SETTING_GUARD : g_setting_list.SETTING_PRISONER);
    Call_PushCell(id);
    Call_PushCell(g_setting_list.SETTING_TYPE);

    Call_Finish(forwardResult);

    g_setting_list.SETTING_STATUS = 0;
    g_setting_list.SETTING_GAME = 0;
    g_setting_list.SETTING_GUARD = 0;

    if(g_setting_list.SETTING_TYPE == 1 || !id || GetClientTeam(id) == CS_TEAM_T) {

		if(IsClientConnected(id))
            SetClientListeningFlags(id, VOICE_MUTED);
            
	}
}

stock SetThirdPerson(client)
{
	if(client > 0 && IsValidEntity(client) && IsClientInGame(client))
	{
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", client);
		SetEntProp(client, Prop_Send, "m_iObserverMode", 1);
		SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 0);
		SetEntProp(client, Prop_Send, "m_iFOV", 120);
	}
}

stock SetFirstPerson(client)
{
	if(client > 0 && IsValidEntity(client) && IsClientInGame(client))
	{
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", client);
		SetEntProp(client, Prop_Send, "m_iObserverMode", 0);
		SetEntProp(client, Prop_Send, "m_bDrawViewmodel", 1);
		SetEntProp(client, Prop_Send, "m_iFOV", 90);
	}
}

stock int is_user_vip(client)
{
    return (GetUserFlagBits(client) & ADMFLAG_CUSTOM3);
}

stock bool lastrequest_ready(int client) {
    if(client > 0 
    && IsValidEntity(client) 
    && IsClientInGame(client)
    && IsPlayerAlive(client) 
    && GetClientTeam(client) == CS_TEAM_T 
    && !g_setting_list.SETTING_STATUS) {
        
        if(GetAliveTeamCount(CS_TEAM_T) == 1 && GetAliveTeamCount(CS_TEAM_CT) >= 1)
            return true;
    }
    return false;
}

stock set_lastrequest(int id, int guard, int gameid, int starter = 0) {

    if(id <= 0 || guard <= 0
    || !IsValidEntity(id) || !IsClientInGame(guard)
    || !IsValidEntity(guard) || !IsClientInGame(id)
    || !IsPlayerAlive(id) || !IsPlayerAlive(guard))
        return;

    if(!g_setting_list.SETTING_TYPE) {
        g_tergame_plays[gameid]++;

        if(g_tergame_list[gameid].TERGAME_FLAGS & TERGAME_SLOWSYSTEM
        || g_setting_list.SETTING_VIEW == 1) {
            g_setting_list.SETTING_SLOWPLAYS += 1;
        }
    }
    
    g_setting_list.SETTING_STATUS = 1;
    g_setting_list.SETTING_PRISONER = id;
    g_setting_list.SETTING_GUARD = guard;

    char strName[2][MAX_NAME_LENGTH];
    GetClientName(id, strName[0], sizeof(strName[]));

    if(guard) {

        if(!starter) {
            SetEntityRenderMode(id, RENDER_GLOW);
            SetEntityRenderMode(guard, RENDER_GLOW);

            SetEntityRenderColor(id, 255, 0, 0, 15);
            SetEntityRenderColor(guard, 0, 0, 255, 15);
        }
        SetEntityHealth(guard, 100);
        SetEntProp(guard, Prop_Send, "m_ArmorValue", 0, 1);
        GetClientName(guard, strName[1], MAX_NAME_LENGTH);

        if(g_setting_list.SETTING_VIEW) {
            SetThirdPerson(id);
            SetThirdPerson(guard);
        }

        if(!g_setting_list.SETTING_TYPE) {
            SlapPlayer(guard, 0, true);
            PrintToChat(guard, "Its your turn on LR.");
        }
    }
    else {
        strcopy(strName[1], MAX_NAME_LENGTH, "Guards");

        for(int i = 0; i <= MaxClients; i++) {
            if(IsClientConnected(i) && IsPlayerAlive(i) && GetClientTeam(i) == CS_TEAM_CT) {
                SetEntityHealth(i, 100);
                SetEntProp(i, Prop_Send, "m_ArmorValue", 0, 1);
            }
        }
    }
    SetEntityHealth(id, 100);
    SetEntProp(id, Prop_Send, "m_ArmorValue", 0, 1);

    if(g_setting_list.SETTING_TYPE
    && starter) {
        if(g_tergame_list[gameid].TERGAME_FLAGS & TERGAME_FRIENDLYFIRE) {
            ServerCommand("sm_cvar jb_tk 1");
        }

        char strStarter[MAX_NAME_LENGTH];
        GetClientName(starter, strStarter, sizeof(strStarter));

        CPrintToChatAll("{green}>nD>{default} Terrorists games started by {green}%s", strStarter);
        CPrintToChatAll("{green}>nD>{default} {red}%s{default} has been pitted against {red}%s{default} in %s", strName[0], strName[1], g_tergame_list[gameid].TERGAME_NAME);

        PrintHintTextToAll("[Tgames] %s vs %s in %s", strName[0], strName[1], g_tergame_list[gameid].TERGAME_NAME);
    }
    else {
        CPrintToChatAll("{green}>nD> {red}%s{default} has chosen to play {red}'%s'{default} against {red}%s{default}. %s", strName[0], g_tergame_list[gameid].TERGAME_NAME,strName[1] , g_setting_list.SETTING_VIEW ? "(3rd view)" : "");
    }

    if(g_tergame_list[g_setting_list.SETTING_GAME].TERGAME_SUBMENU[0]) {
        CPrintToChatAll("{green}>nD>{default} %s{red} %s", g_tergame_list[gameid].TERGAME_NAME, g_setting_list.SETTING_SUB_NAME);
    }

    // Start forwards //
    Action intForwardResult;

    Call_StartForward(g_intForwardStarted);

    Call_PushCell(gameid);
    Call_PushCell(id);
    Call_PushCell(guard);
    Call_PushCell(g_setting_list.SETTING_TYPE);
    Call_PushCell(g_setting_list.SETTING_SUB_ID);

    Call_Finish(intForwardResult);
}

stock int GetAliveTeamCount(int team) {
    int number = 0;
    for (new i = 1; i <= MaxClients; i++) {
        if (IsClientConnected(i) && IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == team) {
            number++;
        }
    }
    return number;
} 