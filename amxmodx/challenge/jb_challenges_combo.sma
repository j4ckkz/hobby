#include <amxmodx>
#include <hamsandwich>
#include <fakemeta>
#include <colorchat>
#include <jb_challenges>

#if AMXX_VERSION_NUM < 183
#include <dhudmessage>
#endif

#pragma semicolon 1

#define TASK_COMBO 123987123928


new g_iComboChallenge;
new bool:g_bInChallenge;

new FW_FM_CMDSTART_POST,
    g_user_progress[33],
    Float:g_delay[33],
    Array:tmp_array;

enum(+=1)
{
    KEY_USE = 0, // 0
    KEY_ATTACK, // 1
    KEY_ATTACK2, // 2
    KEY_MLEFT, // 3
    KEY_MRIGHT, // 4
    KEY_RELOAD, // 5
    KEY_MFORWARD, // 6 
    KEY_MBACKWARD, // 7
    MAX_KEYS
};
new const iComboKeys[] = 
{
    IN_USE,
    IN_ATTACK,
    IN_ATTACK2,
    IN_MOVELEFT,
    IN_MOVERIGHT,
    IN_RELOAD,
    IN_FORWARD,
    IN_BACK
};
new const szComboKeys[][] = 
{
    "USE",
    "MOUSE 1",
    "MOUSE 2",
    "MOVE LEFT",
    "MOVE RIGHT",
    "RELOAD",
    "MOVE FORWARD",
    "MOVE BACKWARD"
};


public plugin_init()
{
    register_plugin("Challenge: Combo", "1.0", "Natsheh");
    g_iComboChallenge = challenge_add("Combo Contest", "You must execute the Combo that will show on the screen.", 15.0);
    FW_FM_CMDSTART_POST = -1;
}

public challenge_started(challengeId)
{
    if(challengeId == g_iComboChallenge)
    {
        g_bInChallenge = true;
        if(FW_FM_CMDSTART_POST == -1)
        {
            // Register forward for cmdstart 
            FW_FM_CMDSTART_POST = register_forward(FM_CmdStart, "fw_CmdStart_post", 1);
        }

        if(!_:tmp_array)
        {
            // temp array
            tmp_array = ArrayCreate(1,1);
        }
        
        // looping for MAX_KEYS
        for(new i; i < MAX_KEYS; i++)
        {
            ArrayPushCell(tmp_array, i);
        }
        
        new szStr[96];
        const maxcombokeys = 6;

        // looping through the maxkeys - max combo keys -> result = remove from combo keys.
        for(new i, maxloop = (ArraySize(tmp_array) - maxcombokeys); i < maxloop; i++)
        {
            // Remove the result from combo keys, (1, 2, 3, ..)
            ArrayDeleteItem(tmp_array, random(ArraySize(tmp_array)));
        }

        // Sort the keys
        ArraySort(tmp_array, "arraysorting_keys");
        

        // Loop through the selected keys after sorting them
        for(new i, maxloop = ArraySize(tmp_array); i < maxloop; i++)
        {
            add(szStr, charsmax(szStr), szComboKeys[ArrayGetCell(tmp_array,i)]);
            add(szStr, charsmax(szStr), "|");
        }
        

        if(szStr[0] != 0)
        {
            szStr[strlen(szStr)-1] = 0;
        }


        set_dhudmessage(255, 0, 0, -1.0, 0.6, 2, 6.0, 9.0);

        new iPlayers[32], iNum, id;
        get_players(iPlayers, iNum, "bc");
        for(new i; i < iNum; i++)
        {
            id = iPlayers[i];

            if(challenge_is_user_in(id))
            {
                show_dhudmessage(id, "Combo Contest- The pattern is..");
                set_task(6.0, "showCombo", id + TASK_COMBO, szStr, charsmax(szStr));
            }
        }
    }
}

public arraysorting_keys(Array:array, item1, item2, const data[], data_size)
{
    if(random_num(0,1) == 0) return -1;
    
    return 1;
}

public showCombo(const szStr[], taskid)
{
    new id = taskid - TASK_COMBO;

    set_dhudmessage(0, 255, 0, -1.0, 0.7, 2, 2.0, 7.0, 0.0, 0.5);

    if(challenge_is_user_in(id))
        show_dhudmessage(id, szStr);

}

public challenge_ended(challengeId)
{
    if(challengeId == g_iComboChallenge)
    {
        g_bInChallenge = false;
        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "c");
        for(new i; i < iNum; i++)
        {
            remove_task(iPlayers[i] + TASK_COMBO);
        }

        if(FW_FM_CMDSTART_POST != -1)
        {
        // remove the forward cmd start
        unregister_forward(FM_CmdStart, FW_FM_CMDSTART_POST, 1);
        FW_FM_CMDSTART_POST = -1;
        }

        if(_:tmp_array > 0)
        {
            ArrayDestroy(tmp_array);
            tmp_array = any:0;
        }
        arrayset(g_user_progress, 0, 33);
    }
}

public fw_CmdStart_post(id, uc_handle, seed)
{
    if(g_bInChallenge && challenge_is_user_in(id))
    {
        if(g_delay[id] >= get_gametime())
            return;
        
        static iButtons; iButtons = get_uc(uc_handle, UC_Buttons);
        static iOldButtons; iOldButtons = pev(id, pev_oldbuttons);
        
        // Check if player is doing the correct combo
        if(iButtons == iComboKeys[ArrayGetCell(tmp_array, g_user_progress[id])])
        {
            g_delay[id] = get_gametime() + 0.1;
            // Increase his progress
            g_user_progress[id]++;
            
            // If player finished the combo
            if(g_user_progress[id] == ArraySize(tmp_array))
            {
                // Restart his progress
                g_user_progress[id] = 0;
                // End the game
                challenge_winner(id);
            }
        }
        // Player Failed
        else if( g_user_progress[id] > 0 && iButtons > 0 && (iOldButtons != iComboKeys[ArrayGetCell(tmp_array, g_user_progress[id]-1)]) )
        {
            g_delay[id] = get_gametime() + 0.1;
            g_user_progress[id] = 0;
        }
    }
}