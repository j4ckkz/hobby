
native challenge_add(name[64], desc[128], cost, Float:duration);
native challenge_winner(index, prize);


native challenge_is_user_in(id);
native challenge_get_total_prize();


native challenge_send_game_duration();

forward challenge_start_pre(game_id);
forward challenge_started(game_id);
forward challenge_ended(gid);
