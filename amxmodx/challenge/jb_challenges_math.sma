#include <amxmodx>
#include <colorchat>
#include <jb_challenges>

#if AMXX_VERSION_NUM < 183
#include <dhudmessage>
#endif

#pragma semicolon 1

#define TASK_MATH 9847392

new g_iMathChallenge;
new bool:g_bInChallenge;
new g_strAnswer[6];
new a, b, c, d, mode;

public plugin_init()
{
    register_plugin("Challenge: Math", "1.0", "Mati & Xalus");

    register_clcmd("say_team", "clcmdSayTeam");

    g_iMathChallenge = challenge_add("Math Contest", "On TEAM CHAT, you must answer to the equation that shows on the screen.", 20.0);
}

public challenge_started(challengeId)
{
    if(challengeId == g_iMathChallenge)
    {
        g_bInChallenge = true;
        g_strAnswer[0] = EOS;

        generateEquation();
        set_dhudmessage(255, 0, 0, -1.0, 0.6, 2, 6.0, 9.0);

        new iPlayers[32], iNum, id;
        get_players(iPlayers, iNum, "bc");
        for(new i; i < iNum; i++)
        {
            id = iPlayers[i];

            if(challenge_is_user_in(id))
            {
                show_dhudmessage(id, "Math Contest - Answer in TEAMCHAT!^nThe equation is...");
                set_task(6.0, "showEquation", id + TASK_MATH);
            }
        }
    }
}

generateEquation()
{
    mode = random_num(1,3);
    switch (mode)
	{
		case 1:
		{
			a = random_num(1,10);
			b = random_num(1,10);
			num_to_str( (a * b), g_strAnswer, charsmax(g_strAnswer));
		}
		case 2:
		{
			a = random_num(1,99);
			b = random_num(1,99);
			num_to_str( (a + b), g_strAnswer, charsmax(g_strAnswer));
		}
		case 3:
		{
			a = random_num(1,9);
			b = random_num(1,9);
			c = random_num(1,9);
			d = random_num(1,9);
			num_to_str( (a + b + c + d), g_strAnswer, charsmax(g_strAnswer));
        }
    }
}

public showEquation(taskid)
{
    new id = taskid - TASK_MATH;
    new szText[32];
    switch(mode)
    {
        case 1: formatex(szText, charsmax(szText), "%d * %d = ?", a, b);
        case 2: formatex(szText, charsmax(szText), "%d + %d = ?", a, b);
        case 3: formatex(szText, charsmax(szText), "%d + %d + %d + %d = ?", a, b, c, d);
    }

    set_dhudmessage(0, 255, 0, -1.0, 0.7, 2, 2.0, 7.0, 0.0, 0.5);
    if(challenge_is_user_in(id))
        show_dhudmessage(id, "%s", szText);

}

public challenge_ended(challengeId)
{
    if(challengeId == g_iMathChallenge)
    {
        g_bInChallenge = false;
        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "c");
        for(new i; i < iNum; i++)
        {
            remove_task(iPlayers[i] + TASK_MATH);
        }
        g_strAnswer[0] = EOS;
    }
}

public clcmdSayTeam(id)
{
    if(g_bInChallenge)
    {
        new Args[10];
        read_args(Args, charsmax(Args));
        remove_quotes(Args);

        if(!Args[0])
            return;

        if(equal(Args, g_strAnswer))
        {
            challenge_winner(id);
            g_strAnswer[0] = EOS;
        }
    }
}
