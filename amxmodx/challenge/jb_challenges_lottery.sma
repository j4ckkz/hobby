#include <amxmodx>
#include <colorchat>
#include <jb_challenges>

#if AMXX_VERSION_NUM < 183
#include <dhudmessage>
#endif

#pragma semicolon 1

#define TASK_LOTTERY 113399874

new g_iLotteryChallenge;
new g_iRandomWinner;

public plugin_init()
{
    register_plugin("Challenge: Lottery", "1.0", "X3");
    g_iLotteryChallenge = challenge_add("Lottery", "A player will be randomly selected as the Lottery Winner", 20.0);
}

public challenge_started(challengeId)
{
    if(challengeId == g_iLotteryChallenge)
    {
        set_dhudmessage(255, 0, 0, -1.0, 0.6, 2, 6.0, 9.0);

        new iPlayers[32], iNum, id;
        get_players(iPlayers, iNum, "bc");
        for(new i; i < iNum; i++)
        {
            id = iPlayers[i];

            if(challenge_is_user_in(id))
            {
                show_dhudmessage(id, "The Lottery Winner Is...");
                set_task(4.0, "showWinner", id + TASK_LOTTERY);
            }
        }
    }
}

public showWinner(taskid)
{
    new id = taskid - TASK_LOTTERY;

    g_iRandomWinner = GetRandomWinner(random_num(1, GetPlayersIn()));

    new szName[32]; get_user_name(g_iRandomWinner, szName, charsmax(szName));
    set_dhudmessage(0, 255, 0, -1.0, 0.7, 2, 2.0, 7.0, 0.0, 0.5);

    if(challenge_is_user_in(id))
        show_dhudmessage(id, "%s", szName);
    
    challenge_winner(g_iRandomWinner);
}

public challenge_ended(challengeId)
{
    if(challengeId == g_iLotteryChallenge)
    {
        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "c");
        g_iRandomWinner = EOS;
    }
}

GetRandomWinner(target_index)
{
    new PlayerIn;
    
    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");
    for (new i; i <= iNum; i++)
    {
        id = iPlayers[i];

        if(challenge_is_user_in(id))
            PlayerIn++;
        
        if (PlayerIn == target_index)
            return id;
    }
    return -1;
}

GetPlayersIn()
{
    new PlayerIn;
    
    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");
    for (new i; i <= iNum; i++)
    {
        id = iPlayers[i];
        if(challenge_is_user_in(id))
            PlayerIn++;
    }
    return PlayerIn;
}