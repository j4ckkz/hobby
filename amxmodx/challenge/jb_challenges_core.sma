#include <amxmodx>
#include <hamsandwich>
#include <colorchat>
#include <cstrike>

#include <ndnet-cash>

#pragma semicolon 1

#define CASH_ENABLED

#define CHALLENGE_NONE -1

const TASK_REMOVEABLE = 384729;

enum _:eForwards
{
    CHALLENGE_USER_JOINED = 0,
    CHALLENGE_STARTED,
    CHALLENGE_ENDED
};

new g_Forward[eForwards], 
    g_ForwardResult;

new Array:g_aChallengeName,
    Array:g_aChallengeDescription,
    Array:g_aChallengeDuration;


new g_LastChallenge = CHALLENGE_NONE;
new g_ChosenChallenge = CHALLENGE_NONE;

new g_iChallengesCount;
new g_iTotalPrize;

new bool:g_bStartGame,
    bool:g_bGameStarted,
    bool:g_bEndingTimer;

new g_JoinedPlayers,
    g_bIsPlayerIn[33];

new g_ChallengeName[32],
    g_ChallengeDesc[128];

new g_fChallengesDelay,
    g_fChallengesEndDelay,
    g_pDisable,
    g_pRequiredPlayers;

native infohud_show(client, hud_id, hud_name[], hud_time, hud_flags);
native infohud_remove(client, hud_id);
native infohud_gettime(client, hud_id);
native infohud_getflag(client, hud_id);
forward infohud_removed(client, hud_id);

enum (<<= 1)
{
    COUNTUP = 1,
    COUNTDOWN,
    PERM
};
enum _:infohud_amounts
{
    _HUDID,
    _AMOUNT
};
enum _:infohud_challenges
{
    CHALLENGE_NAME,
    CHALLENGE_STATUS,
    CHALLENGE_WINNER,
    CHALLENGE_NAMES
};

new g_ChallengesHud_Id[33][infohud_challenges][infohud_amounts];

new bool:g_bDisabled[33];

public plugin_init()
{
    register_plugin("Challenges API", "1.0", "X3");

    register_logevent("roundEnd", 2, "1=Round_End");
    register_logevent("roundEnd", 2, "1&Restart_Round_");

    RegisterHam(Ham_Spawn, "player", "hPlayerSpawnPost", 1);
    RegisterHam(Ham_Killed, "player", "hPlayerKilledPost", 1);

    register_clcmd("say_team /join", "toggleJoin");
    
    g_pRequiredPlayers = register_cvar("challenges_required_players", "3"); // required players to start a challenge 
    g_fChallengesDelay = register_cvar("challenges_start_delay", "20.0"); // delay to start challenges when required players joined
    g_fChallengesEndDelay = register_cvar("challenges_end_delay", "40.0"); // when challenge ends, start next in X time.
    g_pDisable = register_cvar("challenges_disable", "0"); // disable/enable challenges
    
    g_Forward[CHALLENGE_USER_JOINED] = CreateMultiForward("challenge_user_joined", ET_CONTINUE, FP_CELL, FP_CELL);
    g_Forward[CHALLENGE_STARTED] = CreateMultiForward("challenge_started", ET_CONTINUE, FP_CELL);
    g_Forward[CHALLENGE_ENDED] = CreateMultiForward("challenge_ended", ET_CONTINUE, FP_CELL);

}

public plugin_natives()
{
    register_native("challenge_add", "_challlenges_add", 1);
    register_native("challenge_winner", "_challenge_winner", 1);
    register_native("challenge_is_user_in", "_challenge_is_user_in");
    register_native("challenge_user_left_ghost", "_challenges_user_left_ghost");

    g_aChallengeName = ArrayCreate(32, 1);
    g_aChallengeDescription = ArrayCreate(128, 1);
    g_aChallengeDuration = ArrayCreate(4, 1);
}

// Add new challenges 
public _challlenges_add(name[], desc[], Float:duration)
{
    param_convert(1);
    param_convert(2);

    ArrayPushString(g_aChallengeName, name);
    ArrayPushString(g_aChallengeDescription, desc);
    ArrayPushCell(g_aChallengeDuration, duration);

    g_iChallengesCount++;
    return g_iChallengesCount - 1;
}

public _challenge_winner(id)
{
    if(!is_user_connected(id))
        return;
    
    new szName[32]; get_user_name(id, szName, charsmax(szName));
    
    new sText[96];
    formatex(sText, charsmax(sText), "Challenge Winner : %s^nStarting New Challenge in #TIME", szName);

    new iPlayers[32], iNum, index;
    get_players(iPlayers, iNum, "bc");
    for(new i; i < iNum; i++)
    {
        index = iPlayers[i];

        ColorChat(index, GREEN, "^1[^4Challenges^1] %s WON the ^4%s^1 and WON ^4%i^1 nD$.", szName, g_ChallengeName, g_iTotalPrize);
        if(is_user_connected(index) && !is_user_alive(index))
        {
            client_infohud_remove(index, CHALLENGE_STATUS);
            client_infohud_remove(index, CHALLENGE_NAMES);
            g_ChallengesHud_Id[index][CHALLENGE_WINNER][_HUDID] = infohud_show(index, g_ChallengesHud_Id[index][CHALLENGE_WINNER][_HUDID], sText, floatround(get_pcvar_float(g_fChallengesEndDelay), floatround_round), COUNTDOWN);
        }
    }
    #if defined CASH_ENABLED
    new szAuthID[35]; get_user_authid(id, szAuthID, charsmax(szAuthID));
    log_user_ndcash(szAuthID, g_iTotalPrize, "Challenges: Won %s", g_ChallengeName);
    #endif

    resetChallenges();
}

public _challenge_is_user_in(iPlugin, iParams)
{
    return g_bIsPlayerIn[get_param(1)];
}

public _challenges_user_left_ghost(iPlugin, iParams)
{
    new id = get_param(1);

    if(is_user_connected(id) && g_bIsPlayerIn[id])
    {
        g_bIsPlayerIn[id] = false;
        g_JoinedPlayers--;
        
        checkJoinedPlayers();

        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "bc");

        for(new i; i < iNum; i++)
        {
	        challengesHud(iPlayers[i]);
        }
    }
}

public plugin_end()
{
    ArrayDestroy(g_aChallengeName);
    ArrayDestroy(g_aChallengeDescription);
    ArrayDestroy(g_aChallengeDuration);
}

public roundEnd()
{
    resetChallenges();
}

public hPlayerSpawnPost(id)
{
    if(is_user_alive(id))
    {
        // Just to make sure he was in. //
        if(g_bIsPlayerIn[id])
        {
            g_JoinedPlayers--;
            g_bIsPlayerIn[id] = false;
            checkJoinedPlayers();

            new iPlayers[32], iNum;
            get_players(iPlayers, iNum, "bc");

            for(new i; i < iNum; i++)
            {
                challengesHud(iPlayers[i]);
            }
        }
        client_infohud_remove(id, CHALLENGE_NAME);
        client_infohud_remove(id, CHALLENGE_STATUS);
        client_infohud_remove(id, CHALLENGE_WINNER);
        client_infohud_remove(id, CHALLENGE_NAMES);
    }
}

public client_disconnected(id)
{
    // Just make sure he was in //
    if(g_bIsPlayerIn[id])
    {
        g_JoinedPlayers--;
        g_bIsPlayerIn[id] = false;

        checkJoinedPlayers();
    
        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "bc");

        for(new i; i < iNum; i++)
        {
            challengesHud(iPlayers[i]);
        }
    }
    g_bDisabled[id] = false;
    client_infohud_remove(id, CHALLENGE_NAME);
    client_infohud_remove(id, CHALLENGE_STATUS);
    client_infohud_remove(id, CHALLENGE_WINNER);
    client_infohud_remove(id, CHALLENGE_NAMES);
}

public client_putinserver(id)
{
    for(new i = 0; i < infohud_challenges; i++)
    {
        g_ChallengesHud_Id[id][i][_HUDID] = -1;
        g_ChallengesHud_Id[id][i][_AMOUNT] = 0;
    }
    g_bDisabled[id] = false;
    set_task(4.0, "challengesHud", id);
    set_task(5.0, "joinChallenge", id);
}

public hPlayerKilledPost(victim)
{
    challengesHud(victim);
    if(!g_bGameStarted && g_ChosenChallenge != CHALLENGE_NONE)
    {
        challengesHud(victim);
        joinChallenge(victim);
    }
}

public pickChallenge()
{
    new challenge = random(g_iChallengesCount);
    
    if(g_LastChallenge == challenge)
    {
        pickChallenge();
        return;
    }
    g_ChosenChallenge = challenge;

    g_LastChallenge = g_ChosenChallenge;

    ArrayGetString(g_aChallengeName, g_ChosenChallenge, g_ChallengeName, charsmax(g_ChallengeName));
    ArrayGetString(g_aChallengeDescription, g_ChosenChallenge, g_ChallengeDesc, charsmax(g_ChallengeDesc));

    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");

    for(new i; i < iNum; i++)
    {
        id = iPlayers[i];
        challengesHud(id);
        joinChallenge(id);
    }
}

public startChallenge()
{    
    if(g_ChosenChallenge == CHALLENGE_NONE)
        return;

    checkJoinedPlayers();

    g_bGameStarted = true;
    ExecuteForward(g_Forward[CHALLENGE_STARTED], g_ForwardResult, g_ChosenChallenge);
    g_bEndingTimer = true;
    timerHud();
    set_task(ArrayGetCell(g_aChallengeDuration, g_ChosenChallenge), "endChallenge", TASK_REMOVEABLE);

    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");
    for(new i; i < iNum; i++)
    {
        id = iPlayers[i];
        if(g_bIsPlayerIn[id])
        {
            ColorChat(id, GREEN, "^1[^4Challenges^1] %s", g_ChallengeDesc);
        }
    }
}

public endChallenge()
{
    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");

    for(new i; i < iNum; i++)
    {
        id = iPlayers[i];
        client_infohud_remove(id, CHALLENGE_STATUS);
        client_infohud_remove(id, CHALLENGE_NAMES);
        g_ChallengesHud_Id[id][CHALLENGE_WINNER][_HUDID] = infohud_show(id, g_ChallengesHud_Id[id][CHALLENGE_WINNER][_HUDID], "Starting New Challenge In #TIME", floatround(get_pcvar_float(g_fChallengesEndDelay), floatround_round), COUNTDOWN);
    }
    resetChallenges();
}

public resetChallenges()
{
    ExecuteForward(g_Forward[CHALLENGE_ENDED], g_ForwardResult, g_ChosenChallenge);

    remove_task(TASK_REMOVEABLE);
    g_iTotalPrize = 0;
    g_bGameStarted = false;
    g_bStartGame = false;
    g_JoinedPlayers = 0;
    g_ChosenChallenge = CHALLENGE_NONE;
    arrayset(g_bIsPlayerIn, 0, 32);

    new Float:fDelay = get_pcvar_float(g_fChallengesEndDelay);
    
    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");

    for(new i; i < iNum; i++)
    {
        id = iPlayers[i];
        client_infohud_remove(id, CHALLENGE_NAME);
        client_infohud_remove(id, CHALLENGE_STATUS);
        client_infohud_remove(id, CHALLENGE_NAMES);

    }

    set_task(fDelay, "pickChallenge", TASK_REMOVEABLE);
}

public toggleJoin(id)
{
    if(is_user_alive(id) || g_bGameStarted)
        return PLUGIN_HANDLED;
    
    if(!g_bDisabled[id])
    {
        if(g_bIsPlayerIn[id])
        {
            g_JoinedPlayers--;
            g_bIsPlayerIn[id] = false;

            checkJoinedPlayers();
        
            new iPlayers[32], iNum;
            get_players(iPlayers, iNum, "bc");

            for(new i; i < iNum; i++)
            {
                challengesHud(iPlayers[i]);
            }
        }
        g_bDisabled[id] = true;
        ColorChat(id, GREEN, "^1[^4Challenges^1] Disabled the challenges, write ^4/join^1 in Team-Chat again to Enable!");

        return PLUGIN_HANDLED;
    }
    else
    {
        g_bDisabled[id] = false;
        joinChallenge(id);
        ColorChat(id, GREEN, "^1[^4Challenges^1] Enabled the challenge, write ^4/join^1 in Team-Chat again to Disable!", g_ChallengeName);

        return PLUGIN_HANDLED;
    }
    return PLUGIN_CONTINUE;
}

public joinChallenge(id)
{
    if(is_user_alive(id) || g_bIsPlayerIn[id] || g_bGameStarted || g_bDisabled[id])
        return PLUGIN_HANDLED;

    if(g_ChosenChallenge == CHALLENGE_NONE || get_pcvar_num(g_pDisable) == 1)
    {
        ColorChat(id, GREEN, "^1[^4Challenges^1] Theres no challenge ATM. Or plugin is disabled.");
        return PLUGIN_HANDLED;
    }

    ExecuteForward(g_Forward[CHALLENGE_USER_JOINED], g_ForwardResult, id, g_ChosenChallenge);

    if(g_ForwardResult >= PLUGIN_HANDLED)
        return PLUGIN_HANDLED;

    checkJoinedPlayers();
    g_JoinedPlayers++;
    g_bIsPlayerIn[id] = true;

    new iPlayers[32], iNum;
    get_players(iPlayers, iNum, "bc");

    for(new i; i < iNum; i++)
    {
        challengesHud(iPlayers[i]);
    }
    
    checkJoin();

    return PLUGIN_CONTINUE;
}


public checkJoin()
{
    if(g_bStartGame)
        return;
    
    if(g_JoinedPlayers == get_pcvar_num(g_pRequiredPlayers))
    {
        new Float:fDelay = get_pcvar_float(g_fChallengesDelay);
        set_task(fDelay, "startChallenge", TASK_REMOVEABLE);

        g_bEndingTimer = false;
        timerHud();

        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "bc"); // B - dont collect alive. C - dont collect bots

        for(new i; i < iNum; i++)
        {
            ColorChat(iPlayers[i], GREEN, "^1[^4Challenges^1] Starting ^4%s^1 challenge in^4 %.f.0 Seconds.", g_ChallengeName, fDelay);
        }
        g_bStartGame = true;
    }
}

public checkJoinedPlayers()
{
    switch(g_JoinedPlayers)
    {
        case 2..4: g_iTotalPrize = 25;
        case 5..9: g_iTotalPrize = 50;
        case 10..14: g_iTotalPrize = 100;
        case 15..19: g_iTotalPrize = 150;
        case 20..24: g_iTotalPrize = 200;
        case 25..32: g_iTotalPrize = 250;
        default: g_iTotalPrize = 0;
    }

    new iPlayers[32], iNum;
    get_players(iPlayers, iNum, "bc");

    for(new i = 0; i < iNum; i++)
    {
        challengesHud(iPlayers[i]);
    }
}

public challengesHud(id)
{
	if(is_user_connected(id) && !is_user_alive(id))
	{
        new sText[192];
        formatex(sText, charsmax(sText), "Challenge: %s^n%i players with total %inD$ prize", 
			g_ChallengeName,  
			g_JoinedPlayers, 
			g_iTotalPrize);

        g_ChallengesHud_Id[id][CHALLENGE_NAME][_HUDID] = infohud_show(id, g_ChallengesHud_Id[id][CHALLENGE_NAME][_HUDID], sText, 900, COUNTDOWN); 
	}
}

public timerHud()
{
    new Float:fGameStartDelay = get_pcvar_float(g_fChallengesDelay);

    if(g_ChosenChallenge == CHALLENGE_NONE)
        return;
    
    new Float:fGameEndsDelay = ArrayGetCell(g_aChallengeDuration, g_ChosenChallenge);
    new Float:fTimer;

    new iPlayers[32], iNum, id;
    get_players(iPlayers, iNum, "bc");

    for(new i; i < iNum; i++)
    {
        id = iPlayers[i];
        if(is_user_connected(id) && !is_user_alive(id))
        {
	        new sText[64];
	        formatex(sText, charsmax(sText), "%s: #TIME", g_bEndingTimer ? "Ends In" : "Starts In");

	        if(g_bEndingTimer)
	            fTimer = fGameEndsDelay;
	        else
	            fTimer = fGameStartDelay;

	        g_ChallengesHud_Id[id][CHALLENGE_STATUS][_HUDID] = infohud_show(id, g_ChallengesHud_Id[id][CHALLENGE_STATUS][_HUDID], sText, floatround(fTimer, floatround_round), COUNTDOWN); 

        }
    }
}

client_infohud_remove(client, example)
{
    if(g_ChallengesHud_Id[client][example][_HUDID] >= 0)
    {
        infohud_remove(client, g_ChallengesHud_Id[client][example][_HUDID]);
    }
    g_ChallengesHud_Id[client][example][_HUDID] = -1;
}