#include <amxmodx>
#include <colorchat>
#include <jb_challenges>

#if AMXX_VERSION_NUM < 183
#include <dhudmessage>
#endif

#pragma semicolon 1

#define TASK_TYPING 44335577

new g_szWord[32];
new g_iTypingChallenge;
new bool:g_bInChallenge;

public plugin_init()
{
    register_plugin("Challenge: Typing Contest", "1.0", "X3");
    register_clcmd("say_team", "clcmdSayTeam");
    
    g_iTypingChallenge = challenge_add("Typing Contest", "On TEAM CHAT, you must type the word that shows on the screen.", 20.0);
}

public challenge_started(challengeId)
{
    if(challengeId == g_iTypingChallenge)
    {
        g_bInChallenge = true;
        new const szFile[] = "addons/amxmodx/configs/words.txt";

        if(!file_exists(szFile))
        {
            set_fail_state("[Challenges: Typing Contest] The words.txt file doesnt exist!");
        }

        new iLines = file_size(szFile, true);
        read_file(szFile, random_num(0, iLines - 1), g_szWord, charsmax(g_szWord), iLines);

        set_dhudmessage(255, 0, 0, -1.0, 0.6, 2, 6.0, 9.0);

        new iPlayers[32], iNum, id;
        get_players(iPlayers, iNum, "bc");
        for(new i; i < iNum; i++)
        {
            id = iPlayers[i];

            if(challenge_is_user_in(id))
            {
                show_dhudmessage(id, "Typing Contest - answer in TEAMCHAT!^nThe word is...");
                set_task(6.0, "showWord", id + TASK_TYPING);
            }
        }
    }
}

public showWord(taskid)
{
    new id = taskid - TASK_TYPING;

    set_dhudmessage(0, 255, 0, -1.0, 0.7, 2, 2.0, 7.0, 0.0, 0.5);

    if(challenge_is_user_in(id))
        show_dhudmessage(id, "%s", g_szWord);

}

public challenge_ended(challengeId)
{
    if(challengeId == g_iTypingChallenge)
    {
        g_bInChallenge = false;
        arrayset(g_szWord, 0, 32);

        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "c");
        for(new i; i < iNum; i++)
        {
            remove_task(iPlayers[i] + TASK_TYPING);
        }
    }
}

public client_disconnected(id)
{
    remove_task(id + TASK_TYPING);
}

public clcmdSayTeam(id)
{
    if(g_bInChallenge)
    {
        new szArgs[32];
        read_args(szArgs, charsmax(szArgs));
        remove_quotes(szArgs);

        if(equali(g_szWord, szArgs))
        {
            if(is_user_connected(id) && challenge_is_user_in(id))
            {
                challenge_winner(id);
            }
        }
    }
}