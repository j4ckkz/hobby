#include <amxmodx>
#include <colorchat>
#include <jb_challenges>
#include <hamsandwich>
#include <rog>


#if AMXX_VERSION_NUM < 183
#include <dhudmessage>
#endif

#pragma semicolon 1

const TASK_CHICKEN = 710101223;

#define CHICKEN_CLASSNAME "chicken_challenge"
#define CHICKENMODEL "models/chick.mdl"

new g_iChickenChallenge;
new bool:g_bInChallenge;

new g_pChickensAmount;
new g_ChallengeId;

native challenge_user_left_ghost(id);

public plugin_init()
{
    register_plugin("Challenges: Chicken Game", "1.0", "X3");
    register_touch(CHICKEN_CLASSNAME, "player", "pTouchChicken");

    g_iChickenChallenge = challenge_add("Find The Chicken", "As a Ghost. Collect chicken. First to do so wins!", 60.0);
    g_pChickensAmount = register_cvar("challenges_chickens_spawn", "5");
}

public plugin_precache()
{
    precache_model(CHICKENMODEL);
}

public plugin_natives()
{
    register_native("challenge_chicken", "_challenge_chicken");
}

public _challenge_chicken(iPlugin, iParams)
{
    return g_bInChallenge;
}

public challenge_user_joined(id, challengeId)
{
    if(challengeId == g_iChickenChallenge)
    {
        g_ChallengeId = challengeId;

        if(!is_user_ghost(id))
        {
            new szMenu = menu_create("\wDo you want to join \rFind The Chicken\w Challenge?", "hMenu");

            menu_additem(szMenu, "\yYes", "", 0);
            menu_additem(szMenu, "\rNo", "", 0);

            menu_setprop(szMenu, MPROP_EXIT, MEXIT_ALL);
            menu_display(id, szMenu, 0);
        }
    }
}

public checkStatus(id)
{
    if(!is_user_ghost(id) && g_bInChallenge)
    {
        challenge_user_left_ghost(id);
    }
}

public hMenu(id, menu, item)
{
    if(g_ChallengeId != g_iChickenChallenge || g_bInChallenge)
    {
        menu_destroy(menu);
        return PLUGIN_HANDLED;
    }

    switch(item)
    {
        case 0:
        {
            if(g_ChallengeId == g_iChickenChallenge && !g_bInChallenge && !is_user_ghost(id))
            {
                make_user_ghost(id);
                menu_destroy(menu);

                return PLUGIN_HANDLED;
            }
        }
        default: menu_destroy(menu);
    }
    menu_destroy(menu);
    return PLUGIN_HANDLED;
}

public challenge_started(challengeId)
{
    if(challengeId == g_iChickenChallenge)
    {
        g_bInChallenge = true;

        new iPlayers[32], iNum;
        get_players(iPlayers, iNum, "bc");

        for(new i = 0; i < iNum; i++)
        {
            checkStatus(iPlayers[i]);
        }

        new Float:fOrigin[3];
        new iChickens = get_pcvar_num(g_pChickensAmount);

        ROGInitialize(1000.0);
        while(iChickens > 0)
        {
            ROGGetOrigin(fOrigin);
            spawnChicken(fOrigin);
            iChickens--;
        }
    }
}

public spawnChicken(Float:fOrigin[3])
{
    new iEnt = create_entity("info_target");

    entity_set_origin(iEnt, fOrigin);
    entity_set_string(iEnt, EV_SZ_classname, CHICKEN_CLASSNAME);
    entity_set_model(iEnt, CHICKENMODEL);
    entity_set_int(iEnt, EV_INT_solid, SOLID_TRIGGER);

    drop_to_floor(iEnt);
}

public pTouchChicken(iEnt, iToucher)
{
    if(is_valid_ent(iEnt) && is_user_ghost(iToucher))
    {
        challenge_winner(iToucher);
        removeEntites();
    }
}

removeEntites()
{
    new iEnt = -1;

    while( (find_ent_by_class(iEnt, CHICKEN_CLASSNAME)) > 0)
    {
        remove_entity(iEnt);
    }
}

public challenge_ended(challengeId)
{
    if(challengeId == g_iChickenChallenge)
    {
        removeEntites();
        g_bInChallenge = false;
    }
}