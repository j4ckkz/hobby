#include <amxmodx>
#include <engine>
#include <colorchat>

//#define isND

#pragma semicolon 1

const TASK_TRAIL = 1992283617;


#if defined isND
native getNdAttribution(id, str[30]);
#endif

#define MAX_COLORS 9

enum _:eInfo {
    infoName[32],
    infoRGB[3]
};

new const g_aColours[MAX_COLORS][eInfo] = {
    {"Red", {254, 0, 0} },
    {"Blue", {0, 0, 254} },
    {"Green", {0, 254, 0} },
    {"Pink", {254, 127, 254} },
    {"Cyan", {51, 255, 255} },
    {"Yellow", {255, 255, 102} },
    {"Purple", {76, 0, 153} },
    {"Magenta", {255, 0, 255} },
    {"Gold", {255, 215, 0} }
};


enum _:eplayerSettings {

    settingRGB[3],
    //settingSize
    //settingAlpha
};

new g_sPlayerSettings[33][eplayerSettings];

new const g_Sprite[] = "sprites/smoke.spr";
new g_iSpriteIndex;

new bool:g_bHasTrail[33];
new g_pAlpha;

new Trie:g_TrailTrie;

public plugin_init() {

    register_plugin("Trails", "1.0", "X3");
    register_clcmd("say", "hSayCommand");

    g_pAlpha = register_cvar("jb_trail_alpha", "45");

    g_TrailTrie = TrieCreate();
    new trailName[32];
    for(new i = 0; i < MAX_COLORS; i++) {
        log_amx("%i", i);
        copy(trailName, charsmax(trailName), g_aColours[i][infoName]);
        strtolower(trailName);
        TrieSetCell(g_TrailTrie, trailName, i);
    }

}

public plugin_precache() {
    g_iSpriteIndex = precache_model(g_Sprite);
}

public client_disconnected(id) {
    remove_task(id + TASK_TRAIL);
    removeTrail(id);
}

public hSayCommand(id) {

    if(is_user_alive(id) && get_user_flags(id) & ADMIN_LEVEL_D) {

        new sArgs[32];
        read_args(sArgs, charsmax(sArgs));
        remove_quotes(sArgs);

        if(equali(sArgs, "!trail", 5)) {

            new trailColourR[15], trailColourG[15], trailColourB[15];
            parse(sArgs[6], 
            trailColourR, charsmax(trailColourR),
             trailColourG, charsmax(trailColourG), 
             trailColourB, charsmax(trailColourB));

            if(is_str_num(trailColourR)) {

                new r = str_to_num(trailColourR);
                new g = str_to_num(trailColourG);
                new b = str_to_num(trailColourB);


                if(r > 255 || g > 255 || b > 255) {
                    ColorChat(id, GREEN, ">nD> ^1Enter RGB value between 0-255.");
                    return;
                }

                g_sPlayerSettings[id][settingRGB][0] = r;
                g_sPlayerSettings[id][settingRGB][1] = g;
                g_sPlayerSettings[id][settingRGB][2] = b;

                setUserTrail(id);
            }
            else 
            {
                if(!trailColourR[0]) {
                    ColorChat(id, GREEN, ">nD> ^1Usage: !trail <Color or RGB value>. Use !trail help to see available registered colours!");
                    return;
                }

                if(equali(trailColourR, "off")) {
                    if(!g_bHasTrail[id]) {
                        ColorChat(id, GREEN, ">nD> ^1You dont have any trail .-.");
                        return;
                    }
                    removeTrail(id);
                    ColorChat(id, GREEN, ">nD> ^1Trail removed!");
                }

                else if(equali(trailColourR, "help")) {
                    console_print(id, "======== Usage - !trail <color or rgb value> in 'say' chat ========");
                    console_print(id, "======== You can remove your trail by typing '!trail off' ========");
                    console_print(id, "======== Registered Colours ========");

                    for(new c = 0; c < MAX_COLORS; c++) {
                        console_print(id, "#%i ======== %s ========", c+1, g_aColours[c][infoName]);
                    }
                    ColorChat(id, GREEN, ">nD> ^1Information printed to your console!");
                }

                new trailColourID;
                if(TrieGetCell(g_TrailTrie, trailColourR, trailColourID)) {
                    g_sPlayerSettings[id][settingRGB][0] = g_aColours[trailColourID][infoRGB];
                    g_sPlayerSettings[id][settingRGB][1] = g_aColours[trailColourID][infoRGB+1]; 
                    g_sPlayerSettings[id][settingRGB][2] = g_aColours[trailColourID][infoRGB+2];

                    setUserTrail(id);
                }
                else {
                    ColorChat(id, GREEN, ">nD> ^1Sorry, this colour is not registered. Use <^3rgb^1> value instead.");
                }
            }
        }
    }
}

public setUserTrail(id) {

    if(is_user_alive(id)) {

        if(g_bHasTrail[id])
            removeTrail(id);

        applyTrail(id);
        client_print(id, print_chat, "setUserTrail(id)");
        set_task(3.0, "trailLoop", id + TASK_TRAIL);

        new sName[32]; get_user_name(id, sName, charsmax(sName));


        #if defined isND
        ColorChat(0, GREEN, ">nD> ^3%s^1 got a trail! %s", sName, getUserTag(id));
        #endif
    }
}

public trailLoop(taskid) {
    new id = taskid - TASK_TRAIL;

    if(is_user_alive(id) && g_bHasTrail[id]) {
        new Float:fVelocity[3];
        get_user_velocity(id, fVelocity);

        if((fVelocity[0] == 0.0) && (fVelocity[1] == 0.0) && (fVelocity[2] == 0.0)) {
            removeTrail(id);
            applyTrail(id);
        }
    }
    set_task(3.0, "trailLoop", id + TASK_TRAIL);
}


// UAIO trail stocks.

stock applyTrail(id)
{
    message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
    write_byte(TE_BEAMFOLLOW);
    write_short(id); // Player index
    write_short(g_iSpriteIndex); // sprite 
    write_byte(45); // life
    write_byte(10);    // width
    write_byte(g_sPlayerSettings[id][settingRGB][0]); // r
    write_byte(g_sPlayerSettings[id][settingRGB][1]); // g
    write_byte(g_sPlayerSettings[id][settingRGB][2]); // b
    write_byte(get_pcvar_num(g_pAlpha)); // alpha
    message_end();

    g_bHasTrail[id] = true;
    return;
}

stock removeTrail(id)
{
    message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
    write_byte(TE_KILLBEAM);
    write_short(id);
    message_end();
    g_bHasTrail[id] = false;
    return;
}

#if defined isND
stock getUserTag(id) {

    new str[30];
    new temp[30];
    getNdAttribution(id, temp);

    if(temp[0])
        str = temp;
    
    else {
        str = "^x04|nD Regular|^x01";
    }

    return str;
}
#endif
